<?php

namespace Drupal\simple_url_shortner\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class UrlShortnerConfigForm.
 */
class UrlShortnerConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'simple_url_shortner.urlshortnerconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'url_shortner_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('simple_url_shortner.urlshortnerconfig');

    $form['content_type'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content type'),
      '#options' => $this->getAllContentTypes(),
      '#default_value' => $config->get('content_type'),
    ];
    /*$form['taxonomy'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Taxonomy'),
      '#options' => $this->getAllVocabularies(),
      '#default_value' => $config->get('taxonomy'),
    ];*/
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('simple_url_shortner.urlshortnerconfig')
      ->set('asd', $form_state->getValue('asd'))
      ->set('content_type', $form_state->getValue('content_type'))
      //->set('taxonomy', $form_state->getValue('taxonomy'))
      ->save();
  }

  /**
   * Get all content types
   *
   * @return array
   */
  private function getAllContentTypes(){
    $contentTypes = \Drupal::service('entity.manager')->getStorage('node_type')->loadMultiple();
    $contentTypesList = [];
    foreach ($contentTypes as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }

    return $contentTypesList;
  }


  /**
   * Get all Vocabularies available
   *
   * @return array
   */
  private function getAllVocabularies(){
    return taxonomy_vocabulary_get_names();
  }

}
