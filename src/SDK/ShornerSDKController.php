<?php
/**
 * Created by PhpStorm.
 * User: obokar
 * Date: 10/10/2017
 * Time: 13:10
 */

namespace Drupal\simple_url_shortner\SDK;


use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\simple_url_shortner\Controller\UrlShortnerController;

class ShornerSDKController {


  public static function getShortner($id) {
    $response = new AjaxResponse();

    //find node path (node/ID)
    //find pathauto alias
    $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$id);

    //get URL from DB or API
    $inLocalDB = UrlShortnerController::isInDatabase($id);
    $message   = '';
    if ($inLocalDB) {
      $urlDetails = UrlShortnerController::getNodeShortURL($id);
      $message    = $urlDetails->shortner;
    }
    else {
      //ask API for URL
      $api = self::getConvertToShortner($alias);
      $message = 'http://fifo.cc/'.$api[0]->shortner;
      self::saveToShortners('node',$id, $api[0]);
    }

    $response->addCommand(new HtmlCommand('.fifo-url', $message));
    return $response;
  }

  public static function getNodeAlias($nid) {
    return \Drupal::service('path.alias_manager')
                  ->getAliasByPath('/node/' . $nid);
  }

  public static function getConvertToShortner($path) {
    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_URL            => 'http://fifo.cc/api/v2?url=' . $path,
    ]);
    $resp = curl_exec($curl);
    curl_close($curl);
    return json_decode($resp);
  }

  public static function saveToShortners($type, $id, $api) {
    $query = \Drupal::database()->insert('simple_url_shortner');
    $query->fields( [
      'shortner'=> 'http://fifo.cc/'.$api->shortner,
      'reference' => $id,
      'type' => $type
      ]);
    return $query->execute();
  }
}