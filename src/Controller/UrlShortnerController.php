<?php

namespace Drupal\simple_url_shortner\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\simple_url_shortner\ShortURLService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UrlShortnerController.
 */
class UrlShortnerController extends ControllerBase {


  public static function getTest(){
    return 'test';
  }

  public function getShortner($id){

  }

  public static function isInDatabase($nid){

    if(!empty(self::getNodeShortURL($nid))){
      return true;
    }
    return false;
  }

  public static function getNodeShortURL($nid){
    $query = \Drupal::database()->select('simple_url_shortner','short');
    $query->fields('short', ['id','shortner']);
    $query->condition('short.reference', $nid);

    return $query->execute()->fetch();
  }


}
